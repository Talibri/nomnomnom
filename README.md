**UPDATE** Scripts removed as this edge case was patched

These scripts take advantage of a current edge case that allows a user to heal from inventory
1. On the turn the enemy will die
2. When mana is not used or recovered
3. When health is not recovered (unconfirmed)

You identify the HP breakpoints when the enemy will die the next turn and the script
1. Checks enemy HP against that breakpoint every 3.1 seconds
2. Whenever enemy HP drops against the breakpoint consumes target food from inventory
3. 

Also, I **THINK** you need Tick Wrangler to be running while you use this to ensure that the next encounter starts on time.

I tested both scripts on the test server so they should work, but bugs are expected.